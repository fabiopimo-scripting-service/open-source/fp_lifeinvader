fx_version "cerulean"

description "OpenSource Lifeinvader Script by FabioPimo"
author "https://fabiopimo.tebex.io/"
version '1.0.0'
repository 'https://gitlab.com/fabiopimo-scripting-service/open-source/fp_lifeinvader'

lua54 'yes'

games {
  "gta5",
}

ui_page 'web/build/index.html'

client_script "client/**/*"
server_script "server/**/*"

files {
  'web/build/index.html',
  'web/build/**/*',
}
